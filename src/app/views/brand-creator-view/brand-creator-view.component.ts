import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Brand} from '../../models/brand';
import {Router} from '@angular/router';
import {BrandService} from '../../services/brand.service';

@Component({
  selector: 'app-brand-creator-view',
  templateUrl: './brand-creator-view.component.html',
  styleUrls: ['./brand-creator-view.component.css']
})
export class BrandCreatorViewComponent implements OnInit {
  errorMsg: string;
  brand: Brand
  newBrandForm: FormGroup
  constructor(private formBuilder: FormBuilder, private router: Router, private brandService: BrandService) {

    this.brand = new Brand('')
  }

  ngOnInit(): void {
    this._initForm()
  }

  _initForm() {
    this.newBrandForm = this.formBuilder.group({
      'name': ['', Validators.required]
    })
  }

onSubmitNewBrandForm() {
    this.errorMsg = null;
    this.brandService.addBrand(this.newBrandForm.value)
        .then(() => this.router.navigate(['brands']))
        .catch(errMsg => this.errorMsg = errMsg)
}
}
