import { Component, OnInit } from '@angular/core';
import {Brand} from '../../models/brand';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {BrandService} from '../../services/brand.service';

@Component({
  selector: 'app-brand-editor-view',
  templateUrl: './brand-editor-view.component.html',
  styleUrls: ['./brand-editor-view.component.css']
})
export class BrandEditorViewComponent implements OnInit {
  errorMsg: string;
  brand: Brand
  editBrandForm: FormGroup
  constructor(private formBuilder: FormBuilder, private router: Router, private brandService: BrandService, private route: ActivatedRoute) {
    this.brand = new Brand('');
  }

  ngOnInit(): void {
    const id = this.route.snapshot.params.id;

    this.brandService.getById(id)
        .then((brand: Brand) => this.brand = brand)
    this._initForm()
  }

  _initForm() {
    this.editBrandForm = this.formBuilder.group({
      'name': ['', Validators.required]
    })
  }

  onSubmitEditBrandForm() {
    this.errorMsg = null;
    this.brandService.edit(this.brand)
        .then(() => this.router.navigate(['brands']))
        .catch(errMsg => this.errorMsg = errMsg)
  }
}

