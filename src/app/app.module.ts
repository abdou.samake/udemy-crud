import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app.routing';

import {AppComponent} from './app.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {FooterComponent} from './components/footer/footer.component';

import {CommonModule} from '@angular/common';
import {NouisliderModule} from 'ng2-nouislider';
import {JwBootstrapSwitchNg2Module} from 'jw-bootstrap-switch-ng2';
import {HomeViewComponent} from './views/home-view/home-view.component';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import * as firebase from 'firebase';
import { SignupFormComponent } from './components/signup-form/signup-form.component';
import { DashboardViewComponent } from './views/dashboard-view/dashboard-view.component';
import {AuthService} from './services/auth.service';
import {AngularFireAuthGuard} from '@angular/fire/auth-guard';
import { SignBaseViewComponent } from './views/sign-base-view/sign-base-view.component';
import {SigninFormComponent} from './components/signin-form/signin-form.component';
import { PageAdminComponent } from './page-admin/page-admin.component';
import { BrandCreatorViewComponent } from './views/brand-creator-view/brand-creator-view.component';
import { BrandEditorViewComponent } from './views/brand-editor-view/brand-editor-view.component';
import { BrandsViewComponent } from './views/brands-view/brands-view.component';
import { ModalConfirmDeleteComponent } from './modal-confirm-delete/modal-confirm-delete.component';
import {BrandService} from './services/brand.service';
// firebase.initializeApp(environment.firebaseConfig);

@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        FooterComponent,
        HomeViewComponent,
        SigninFormComponent,
        SignupFormComponent,
        DashboardViewComponent,
        SignBaseViewComponent,
        PageAdminComponent,
        BrandCreatorViewComponent,
        BrandEditorViewComponent,
        BrandsViewComponent,
        ModalConfirmDeleteComponent,
    ],
    imports: [
        BrowserModule,
        NgbModule,
        FormsModule,
        RouterModule,
        CommonModule,
        AppRoutingModule,
        ReactiveFormsModule,
        NouisliderModule,
        JwBootstrapSwitchNg2Module,
        AngularFireModule.initializeApp(environment.firebaseConfig)
    ],
    providers: [
        AuthService,
        AngularFireAuthGuard,
        BrandService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
