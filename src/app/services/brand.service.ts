import { Injectable } from '@angular/core';
import {Brand} from '../models/brand';
import {AngularFirestore} from '@angular/fire/firestore';
import {BehaviorSubject, Observable, ReplaySubject} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BrandService {
brands: ReplaySubject<Array<Brand>>
  constructor(private afs: AngularFirestore) {

  this.brands = new ReplaySubject<Array<Brand>>();
  }
  getAll() {
    this.afs.collection('brands')
        .snapshotChanges()
        .pipe(
            map(actions => {
              return actions.map(a => {
                const brand = a.payload.doc.data() as Brand;
                brand.id = a.payload.doc.id;
                return brand;
              })
            })
        )
        .subscribe((brands: Array<Brand>) => this.brands.next(brands))
  }
  getById(brandId) {
    return new Promise(
        (res, rej) => {
            this.afs
                .collection('brands')
                .doc(brandId)
                .get()
                .pipe(
                    map(data => {
                        const brand = data.data() as Brand;
                        brand.id = data.id;
                        return brand;
                    })
            ).subscribe((brand: Brand) => res(brand))
        }
    )
  }

  addBrand(brand: Brand) {
    return new Promise(
        (res, rej) => {
          this.afs.collection('brands').add(brand)
              .then(() => res())
              .catch(err => rej(err))
        }
    )
  }
  edit(brand: Brand) {
    return new Promise(
        (res, rej) => {
            this.afs.collection('brands')
                .doc(brand.id)
                .update({
                    name: brand.name
                })
                .then(res)
                .catch(err => rej(err))
        }
    )
  }
  delete(brandId) {
    return new Promise(
        (res, rej) => {
            this.afs.collection('brands')
                .doc(brandId)
                .delete()
                .then(res)
                .catch(err => rej(err))
        }
    )
  }
}
