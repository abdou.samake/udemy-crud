import { Injectable } from '@angular/core';
import {BehaviorSubject, Subscription} from 'rxjs';
import * as firebase from 'firebase';
import {User} from '../models/user';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
    token: BehaviorSubject<string>
    currentUser: any;
    isAdmin: BehaviorSubject<boolean>
  constructor(private afa: AngularFireAuth, private afs: AngularFirestore) {
  this.token = new BehaviorSubject<string>(null);
  this.isAdmin = new BehaviorSubject<boolean>(false);
  }
  signUp(newUser: User) {
  return new Promise(
      (res, rej) => {
        this.afa.createUserWithEmailAndPassword(newUser.email, newUser.password)
            .then((currentUser) => {
              this.token.next(currentUser.user.refreshToken);
              this.currentUser = currentUser;
                this.isCurrentUserAdmin();
              newUser.id = currentUser.user.uid;
              this.afs.collection('user').doc(newUser.id)
                  .set(newUser.toPlainObj())
                  .then(() => res())
            })
            .catch((err) => {
              if (err.code === 'auth/email-already-in-use') {
                rej(`L'adress email est déja utilisé.`)
              }

              if (err.code === 'auth/invalid-email') {
                rej(`L'adress email est invalid.`)
              }

              if (err.code === 'auth/operation-not-allowed') {
                rej(`une erreur est survenue.`)
              }

              if (err.code === 'auth/weak-password') {
                rej(`Le mot de passe n'est pas sécurisé.`)
              }
              rej(`une erreur est survenue.`)
            })
      }
  )
  }

    signIn(email: string, password: string) {
    return new Promise(
        (res, rej) => {
            this.afa.signInWithEmailAndPassword(email, password)
                .then((currentUser) => {
                    this.token.next(currentUser.user.refreshToken);
                    this.currentUser = currentUser;
                    this.isCurrentUserAdmin()

                    res();
                })
                .catch(err => {
                    if (err.code === 'auth/invalid-email') {
                        rej(`L'adresse email est invalid.`)
                    }

                    if (err.code === 'auth/user-not-found') {
                        rej(`L'utilisateur n'existe pas.`)
                    }

                    if (err.code === 'auth/user-disabledl') {
                        rej(`L'utilisateur a été banni.`)
                    }

                    if (err.code === 'auth/wrong-password') {
                        rej(`Le mot de passe est incorrect.`)
                    }
                    rej('Une erreur est survenue')
                })
        }
    )
    }
    logOut() {
    return new Promise(
        (res, rej) => {
            this.afa.signOut()
                .then(() => {
                    this.token.next(null);
                    this.currentUser = null;
                    this.isAdmin.next(false);
                    res()
                })
        }
    )
    }
    getToken() {
    if (!this.token.getValue()) {
        this.afa.authState.subscribe(
            (user) => {
                if (user && user.uid) {
                    this.token.next(user.refreshToken);
                    return this.token;
                }
            }
        )
    }
    return this.token;
    }

    /**
     * Methode for retrieve the current user and chech if his emailm is an admin email
     */
    isCurrentUserAdmin() {
        if (this.currentUser) {
            ['admin@admin.com'].includes(this.currentUser.email) ? this.isAdmin.next(true) : this.isAdmin.next(false);
        } else {
            this.afa
                .user.subscribe(user => {
                    if (user) {
                        this.currentUser = user;
                        ['admin@admin.com'].includes(this.currentUser.email) ? this.isAdmin.next(true) : this.isAdmin.next(false);
                    } else {
                        this.isAdmin.next(false);
                    }
            });
        }
    }
}
