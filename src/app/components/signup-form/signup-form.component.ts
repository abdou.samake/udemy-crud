import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../models/user';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent implements OnInit {
signupForm: FormGroup
  errorMsg: string
  user: User
  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {
  this.user = new User('', '', '', '');
  }

  ngOnInit(): void {
  this._initForm()
  }
  onSubmitSignupForm() {
this.errorMsg = null;
this.authService.signUp(this.user)
    .then(() => {
      this.router.navigate(['dashboard'])
    })
    .catch((errMsg) => this.errorMsg = errMsg);
}
  _initForm() {
  this.signupForm = this.formBuilder.group({
    'firstName': ['', Validators.required],
    'lastName': ['', Validators.required],
    'email': ['', Validators.required],
    'password': ['', Validators.required]
  })
  }
}
