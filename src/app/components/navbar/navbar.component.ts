import {Component, OnInit, ElementRef} from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {Subscription} from 'rxjs';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
    private toggleButton: any;
    private sidebarVisible: boolean;
    isAdmin: boolean;
    isAdminSub: Subscription;
    tokenSub: Subscription
    isAnonymous: boolean = true;
    constructor(private element: ElementRef, private authService: AuthService, private router: Router, private location: Location) {
        this.sidebarVisible = false;
    }

    ngOnInit() {
        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
        // Observe the token
        this.tokenSub = this.authService.getToken()
            .subscribe(
                token => this.isAnonymous = !token
            )
        this.authService.isAdmin
            .subscribe((isAdmin: boolean) => this.isAdmin = isAdmin);
        this.authService.isCurrentUserAdmin()
    }
    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const html = document.getElementsByTagName('html')[0];

        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);
        html.classList.add('nav-open');

        this.sidebarVisible = true;
    };

    sidebarClose() {
        const html = document.getElementsByTagName('html')[0];

        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        html.classList.remove('nav-open');
    };

    sidebarToggle() {
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
    };

    onClickLogOut() {
        this.isAdmin = false;
        this.authService.logOut()
            .then(() => this.router.navigate(['']))
    }

    /**
     * Select the class if we are on a specific page or not
     */
    grtTypeHeader() {
        const path = this.location.prepareExternalUrl(this.location.path()).slice(1).split('/')[1]

        if (['brands', 'brand'].includes(path)) {
            return 'bg-dark';
        }
        return 'navbar-transparent';
    }

    ngOndestroy() {
        this.tokenSub.unsubscribe();
        this.isAdminSub.unsubscribe()
    }
}
