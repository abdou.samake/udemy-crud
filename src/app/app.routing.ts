import {NgModule} from '@angular/core';
import {CommonModule,} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {Routes, RouterModule} from '@angular/router';

import {HomeViewComponent} from './views/home-view/home-view.component';
import {SignBaseViewComponent} from './views/sign-base-view/sign-base-view.component';
import {SignupFormComponent} from './components/signup-form/signup-form.component';
import {DashboardViewComponent} from './views/dashboard-view/dashboard-view.component';
import {AngularFireAuthGuard, redirectLoggedInTo, redirectUnauthorizedTo} from '@angular/fire/auth-guard';
import {map} from 'rxjs/operators';
import {PageAdminComponent} from './page-admin/page-admin.component';
import {BrandsViewComponent} from './views/brands-view/brands-view.component';
import {BrandCreatorViewComponent} from './views/brand-creator-view/brand-creator-view.component';
import {BrandEditorViewComponent} from './views/brand-editor-view/brand-editor-view.component';

const redirectLoggedInToDashboard = () => redirectLoggedInTo(['dashboard'])
const redirectunauthoriszdTologin = () => redirectUnauthorizedTo(['signin'])
const redirectUnauthorizedorNotAdminToDash = () =>
    map((user: any) => {
        if (!user) {
            return ['signin'];
        } else if (['admin@admin.com'].includes(user.email)) {
            return true;
        }
        return ['dasboard'];
    })

const routes: Routes = [
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {
        path: 'home',
        component: HomeViewComponent
    },

    {
        path: 'signin',
        component: SignBaseViewComponent,
        canActivate: [AngularFireAuthGuard],
        data: {authGuardPipe: redirectLoggedInToDashboard}
        },
    {
        path: 'signup',
        component: SignBaseViewComponent,
        canActivate: [AngularFireAuthGuard],
        data: {authGuardPipe: redirectLoggedInToDashboard}
        },
    {
        path: 'dashboard',
        component: DashboardViewComponent,
        data: {authGuardPipe: redirectunauthoriszdTologin}
        },
    {
        path: 'pageAdmin',
        component: PageAdminComponent,
        canActivate: [AngularFireAuthGuard],
        data: {authGuardPipe: redirectUnauthorizedorNotAdminToDash}
        },

    {
        path: 'brands',
        component: BrandsViewComponent,
        canActivate: [AngularFireAuthGuard],
        data: {authGuardPipe: redirectunauthoriszdTologin}
    },

    {
        path: 'brand/new',
        component: BrandCreatorViewComponent,
        canActivate: [AngularFireAuthGuard],
        data: {authGuardPipe: redirectunauthoriszdTologin}
    },
    {
        path: 'brand/edit/:id',
        component: BrandEditorViewComponent,
        canActivate: [AngularFireAuthGuard],
        data: {authGuardPipe: redirectunauthoriszdTologin}
    },
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes, {
            useHash: true
        })
    ],
    exports: [],
})
export class AppRoutingModule {
}
